#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <fstream>
#include <sstream>
#include <map>
#include <list>
#include <set>
#include <algorithm>
#include <ctime>
#include <chrono>
#include "Szam.h"

vector<long int> primek;


bool prim(long long int sz)
{
	if (sz < 2)
		return false;

	for (int i = 0; i < primek.size() && primek[i] * primek[i] <= sz; i++)//could be better (ordered -> ln n search + primek[i]-primek[i] <= sz)
		if (sz % primek[i] == 0)
			return false;
	return true;
}

void primfeltolt(long long int meddig, int mettol = 2)
{
	if (mettol < 3)
	{
		primek.push_back(2);
		mettol = 3;
	}
	int i = mettol - mettol % 2 + 1;
	while (i <= meddig)
	{
		if (prim(i))
			primek.push_back(i);
		i += 2;
	}
}

bool ispalindrom(unsigned long long int k)
{
	int n = (int)log10((double)k) + 1;
	for (int i = 0; i < n / 2; i++)
		if (k / (unsigned long long int)pow((double)10, i) % 10 != k / (unsigned long long int)pow((double)10, n - i - 1) % 10)
			return false;
	return true;
}

int getcharval(char c)
{
	switch (c){
	case '0':
		return 0;
	case '1':
		return 1;
	case '2':
		return 2;
	case '3':
		return 3;
	case '4':
		return 4;
	case '5':
		return 5;
	case '6':
		return 6;
	case '7':
		return 7;
	case '8':
		return 8;
	case '9':
		return 9;
	}
	return -1;
}

vector<long> osztok(long long int szam)
{
	vector<long int> vec;
	long long int j;
	for (j = 1; j * j < szam; j++)
		if (szam / j * j == szam)
		{
			vec.push_back( (long) j );
			vec.push_back( (long) (szam / j) );
		}
	if (j *j == szam)
		vec.push_back((long) j);

	return vec;
}

vector<long int> prop_div(long long int szam)
{
	vector<long int> vec;
	long long int j;
	for (j = 2; j * j < szam; j++)
		if (szam / j * j == szam)
		{
			vec.push_back( (long) j );
			vec.push_back( (long)(szam / j) );
		}
	if (j *j == szam)
		vec.push_back( (long)j );

	if (szam > 1)
		vec.push_back(1);

	return vec;
}

vector<pair<long, int>> primfelbont(long long int szam)
{
	if (szam < 2)
		return vector<pair<long, int>>();

	vector<pair<long, int>> ret;

	for (size_t i = 0; i < primek.size() && primek[i] <= szam; i++)
	{
		int db = 0;
		while (szam % primek[i] == 0)
		{
			db++;
			szam /= primek[i];
		}
		if (db)
			ret.push_back(pair<long, int>(primek[i], db));
	}

	return ret;
}

pair<long, long> egyszerusit(long n, long d)
{
	vector<pair<long, int>> pf_n = primfelbont(n);
	vector<pair<long, int>> pf_d = primfelbont(d);
	for (int i = 0; i < pf_n.size(); i++)
		for (int j = 0; j < pf_d.size() && pf_d[j].first <= pf_n[i].first; j++)
			if (pf_n[i].first == pf_d[j].first)
			{
				int kisebb = min(pf_n[i].second, pf_d[j].second);
				pf_n[i].second -= kisebb;
				pf_d[j].second -= kisebb;
			}
	pair<long, long> ret = pair<long, long>(1, 1);
	for (int i = 0; i < pf_n.size(); i++)
		ret.first *=  (long) pow((double)pf_n[i].first, pf_n[i].second);
	for (int j = 0; j < pf_d.size(); j++)
		ret.second *= (long) pow((double)pf_d[j].first, pf_d[j].second);
	return ret;
}

long long int fact(int x)
{
	int val = 1;
	for (int i = 1; i <= x; i++)
		val *= i;
	return val;
}


unsigned long long int baseNForm(unsigned long long int x, long long int N)//N<=10
{
	unsigned long long int ret = 0;
	unsigned long long int szorzo = 1;
	while (x > 0)
	{
		ret += szorzo * (x % N);
		szorzo *= 10;
		x /= N;
	}
	return ret;
}

bool isPanDigitalFromAToB(int num, int A, int B)
{
	if (A < 0 || B > 9 || A > B)
		return false;

	std::set<short> jegyek;

	while (num > 0)
	{
		if (num % 10 == 0 || num % 10 < A || num % 10 > B || jegyek.find(num % 10) != jegyek.end())
			return false;

		jegyek.insert(num % 10);
		num /= 10;
	}

	return true;
}

int triangle_pos = 0;
long triangle_act = 0;
std::set<int> triangle_nums;

bool isTriangleNum(long num)
{
	while (num > triangle_act)
	{
		triangle_pos++;
		triangle_act = triangle_pos * (triangle_pos + 1) / 2;
		triangle_nums.insert(triangle_act);
	}

	return triangle_nums.find(num) != triangle_nums.end();
}

bool divisible(long long int a, long long int b)
{
	return a % b == 0;
}