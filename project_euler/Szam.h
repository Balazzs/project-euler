#pragma once
#include <vector>
#include <sstream>

using namespace std;



class Szam
{
protected:
	vector<char> v;

public:
	static int getcharval(char c)
	{
		switch(c){
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
		}
	}


	Szam(void)
	{
	}

	Szam(int n)
	{
		for(int i = 0; i <= log10((double)n); i++)
			v.push_back((n / (int)pow((double) 10, i) ) % 10);
	}

	Szam(string str)
	{
		for(size_t i = str.size()-1; i >= 0 ; i--)
			v.push_back(getcharval(str[i]));
	}

	~Szam(void)
	{
	}

	Szam operator+(Szam b)
	{
		Szam ret;
		
		Szam& c = (this->v.size() <= b.v.size()) ? *this : b;
		Szam& d = (this->v.size() <= b.v.size()) ? b : *this;

		int maradek = 0;
		for(int i = 0; i < c.v.size(); i++)
		{
			int temp = (maradek + c.v[i] + d.v[i]);
			ret.v.push_back((temp % 10));
			maradek = temp / 10;
		}

		for(size_t i = c.v.size(); i < d.v.size(); i++)
		{
			int temp = (maradek + d.v[i]);
			ret.v.push_back((temp % 10));
			maradek = temp / 10;
		}

		while(maradek > 0)
		{
			ret.v.push_back((maradek % 10));
			maradek /= 10;
		}

		return ret;
	}

	string ToString()
	{
		ostringstream ss;
		for(size_t i = v.size()-1; i >= 0; i--)
			ss << (int) v[i];
		return ss.str();
	}

	size_t Jegyek()
	{
		return v.size();
	}
};

